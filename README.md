# Programming for Data Analytics


Introduction.

Hello World.

Variables: numbers, strings, Booleans,

Operators: comparison operators, logical

Operators: arithmetic operators.

Decision making / Flow control.

Loops: For, do.

Functions.

Validations

String Manipulation.

Collections (Lists, Tuples, Sets, Dictionaries).

Multi-dimensional data structures.

Pandas.

Object Orientation in Python.

Database integration.

Graphic User Interfaces.

Use of external APIs
