import numpy as np


# functions to do the actual arithmetic
def add(x, y):
    return x + y


def sub(x, y):
    return x - y


def mult(x, y):
    return x * y


def div(x, y):
    return x / y


# defining a custom error to handle division by zero
class Error(Exception):
    pass


class DivisionByZero(Error):
    pass


# MAIN MENU.
# Prints the options then stores the user input in a variable
while True:
    print("Please select an option\n\
            A: Addition\n\
            B: Subtraction\n\
            C: Multiplication\n\
            D: Division\n\
            E: Exit")
    choice = input().upper()
    print()

# ADDITION
    if choice == "A":
        while True:
            try:    # ensures only numbers entered
                q = "How many numbers would you like to add together? "
                n = int(input(q))
                ans = np.zeros(2)
                # the fn only adds two arguments
                # so we sum the user inputs successively
                for i in range(n):
                    # add ans[0] and the input number
                    ans[1] = add(ans[0], float(input("Enter a number: ")))
                    # then move this result to ans[0] and repeat n times
                    ans[0] = ans[1]
                print("The total is:", ans[0], "\n")
                break
            except ValueError:
                print("Please enter a number only.\n")

# SUBTRACTION
    elif choice == "B":
        while True:
            # the while loops stop the user from being sent back to
            # main menu if there is an input error at this stage
            try:    # subtraction is fairly simple. Just a function call
                ans = sub(float(input("Enter a number: ")),
                          float(input("Enter a number: ")))
                print("The total is:", ans, "\n")
                break
            except ValueError:    # checking for numbers
                print("Please enter a number only.\n")

# MULTIPLICATION
    elif choice == "C":    # this is very similar to addition
        while True:
            try:
                q = "How many numbers would you like to multiply together? "
                n = int(input(q))
                # the only difference is we have to initialise the array with
                # a non zero value in ans[0] to avoid the final answer being 0
                ans = np.array([1, 0])
                for i in range(n):
                    ans[1] = mult(ans[0], float(input("Enter a number: ")))
                    ans[0] = ans[1]
                print("The total is:", ans[0], "\n")
                break
            except ValueError:
                print("Please enter a number only.\n")

# DIVISION
    elif choice == "D":
        while True:
            try:
                # in order to catch the division by zero error
                # the inputs need individual variables
                num = float(input("Enter a number: "))
                denom = float(input("Enter a number: "))
                if denom == 0:    # catching the error
                    raise DivisionByZero
                ans = div(num, denom)
                print("The total is:", ans, "\n")
                break
            except DivisionByZero:
                print("Division by zero not allowed!\n")

# CLOSING THE PROGRAMME
    elif choice == "E":
        print("Thanks for using the super amazing calculator!")
        break

    else:    # simple error handling for menu input
        print("Please enter \"A\", \"B\", \"C\", \"D\" or \"E\"\n")
